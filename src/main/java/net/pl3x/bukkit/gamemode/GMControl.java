package net.pl3x.bukkit.gamemode;

import net.pl3x.bukkit.gamemode.command.GMAdventure;
import net.pl3x.bukkit.gamemode.command.GMCreative;
import net.pl3x.bukkit.gamemode.command.GMSpectate;
import net.pl3x.bukkit.gamemode.command.GMSurvival;
import net.pl3x.bukkit.gamemode.configuration.Config;
import net.pl3x.bukkit.gamemode.configuration.Lang;
import net.pl3x.bukkit.gamemode.listener.GMListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class GMControl extends JavaPlugin {
    private final Logger logger;

    public GMControl() {
        logger = new Logger(this);
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        getCommand("gma").setExecutor(new GMAdventure());
        getCommand("gmc").setExecutor(new GMCreative());
        getCommand("gms").setExecutor(new GMSurvival());
        getCommand("gmsp").setExecutor(new GMSpectate());

        Bukkit.getPluginManager().registerEvents(new GMListener(this), this);

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        getLog().info(getName() + " disabled.");
    }
}
