package net.pl3x.bukkit.gamemode.command;

import net.pl3x.bukkit.gamemode.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class GMAdventure implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers()
                    .stream().filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 1) {
            Lang.send(sender, Lang.BAD_SYNTAX);
            return false;
        }

        Player target;
        if (args.length == 1) {
            if (!sender.hasPermission("command.gmadventure.others")) {
                Lang.send(sender, Lang.NO_PERMISSION);
                return true;
            }
            //noinspection deprecation (fucking bukkit)
            target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                Lang.send(sender, Lang.PLAYER_NOT_FOUND);
                return true;
            }
        } else {
            if (!(sender instanceof Player)) {
                Lang.send(sender, Lang.PLAYER_COMMAND);
                return true;
            }
            if (!sender.hasPermission("command.gmadventure")) {
                Lang.send(sender, Lang.NO_PERMISSION);
                return true;
            }
            target = (Player) sender;
        }

        Lang.send(target, Lang.GAMEMODE_SET
                .replace("{target}", "Your")
                .replace("{gamemode}", "Adventure"));
        target.setGameMode(GameMode.ADVENTURE);

        // notify sender if they cant see the spy mode
        if (args.length == 1 && !sender.hasPermission("gamemode.spy") && !sender.getName().equals(target.getName())) {
            Lang.send(sender, Lang.GAMEMODE_SET
                    .replace("{target}", target.getName())
                    .replace("{gamemode}", "Adventure")
                    .replace("{world}", target.getWorld().getName()));
        }
        return true;
    }
}
