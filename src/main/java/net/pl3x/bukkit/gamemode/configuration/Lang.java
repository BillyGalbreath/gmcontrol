package net.pl3x.bukkit.gamemode.configuration;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lang {
    public static String NO_PERMISSION;
    public static String BAD_SYNTAX;
    public static String PLAYER_COMMAND;
    public static String PLAYER_NOT_FOUND;

    public static String GAMEMODE_SET;
    public static String GAMEMODE_SPY;

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        plugin.saveResource(Config.LANGUAGE_FILE, false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        NO_PERMISSION = config.getString("no-permission", "&4You do not have permission for this command!");
        BAD_SYNTAX = config.getString("bad-syntax", "&4Bad command syntax!");
        PLAYER_COMMAND = config.getString("player-command", "&4This command is only available to players.");
        PLAYER_NOT_FOUND = config.getString("player-not-found", "&4Cannot find that player!");

        GAMEMODE_SET = config.getString("gamemode-set", "&e{target} gamemode was set to {gamemode} mode.");
        GAMEMODE_SPY = config.getString("gamemode-spy", "&e{player} has changed gamemode to {gamemode} in world {world}.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
