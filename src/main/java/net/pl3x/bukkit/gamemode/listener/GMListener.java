package net.pl3x.bukkit.gamemode.listener;

import net.pl3x.bukkit.gamemode.GMControl;
import net.pl3x.bukkit.gamemode.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;

public class GMListener implements Listener {
    private final GMControl plugin;

    public GMListener(GMControl plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onGameModeChange(PlayerGameModeChangeEvent event) {
        Player player = event.getPlayer();
        if (player.isDead() || player.getHealth() <= 0D) {
            event.setCancelled(true);
            return;
        }

        GameMode gm = event.getNewGameMode();
        if (!player.hasPermission("command.gm" + (gm == GameMode.SPECTATOR ? "spectate" : gm.name().toLowerCase()))) {
            event.setCancelled(true);
            plugin.getLog().warn(player.getName() + " tried to change gamemode to " + gm + " but does not have permission");
            return;
        }

        String gamemodeChangedMsg = Lang.GAMEMODE_SPY
                .replace("{player}", player.getName())
                .replace("{gamemode}", gm.name())
                .replace("{world}", player.getWorld().getName());

        plugin.getLog().debug(gamemodeChangedMsg);

        for (Player target : Bukkit.getOnlinePlayers()) {
            if (!target.hasPermission("gamemode.spy")) {
                continue;
            }

            if (target.equals(player)) {
                continue;
            }

            Lang.send(target, gamemodeChangedMsg);
        }
    }
}
